
#include <string>
#include <iostream>
#include "jsoncpp/json/json.h"
#include <vector>
#include <sstream>

#include <pistache/net.h>
#include <pistache/http.h>
#include <pistache/peer.h>
#include <pistache/http_headers.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>
#include <pistache/common.h>

#include <signal.h>

#include "Header Files/culoare.h"
#include "Header Files/muzica.h"
#include "Header Files/alarma.h"
#include "Header Files/light.h"

using namespace Pistache;

// Definition of the LightEndpoint class 
class LightEndpoint {
public:
    explicit LightEndpoint(Address addr)
        : httpEndpoint(std::make_shared<Http::Endpoint>(addr))
    { }

    // Initialization of the server. Additional options can be provided here
    void init(size_t thr = 2) {
        auto opts = Http::Endpoint::options()
            .threads(static_cast<int>(thr));
        auto opts2 = Http::Endpoint::options().flags(Tcp::Options::ReuseAddr);
        httpEndpoint->init(opts2);
        httpEndpoint->init(opts);
        // Server routes are loaded up
        setupRoutes();
    }

    // Server is started threaded.  
    void start() {
        httpEndpoint->setHandler(router.handler());
        httpEndpoint->serveThreaded();
    }

    // When signaled server shuts down
    void stop(){
        httpEndpoint->shutdown();
    }

private:
    void setupRoutes() {
        using namespace Rest;

        // Defining various endpoints
        //GET ROUTES
        Routes::Get(router, "/get/status", Routes::bind(&LightEndpoint::getStatus, this));
        Routes::Get(router, "/get/intensity", Routes::bind(&LightEndpoint::getIntensity, this));
        Routes::Get(router, "/get/culoare", Routes::bind(&LightEndpoint::getCuloare, this));

        // POST ROUTES
        Routes::Post(router, "/power", Routes::bind(&LightEndpoint::power, this));
        Routes::Post(router, "/colors", Routes::bind(&LightEndpoint::colors, this));
        Routes::Post(router, "/music", Routes::bind(&LightEndpoint::music, this));
        Routes::Post(router, "/alarm", Routes::bind(&LightEndpoint::alarma, this));

        // PUT ROUTES
        Routes::Put(router, "/intensity", Routes::bind(&LightEndpoint::intensity, this));

    }

    void power(const Rest::Request& request, Http::ResponseWriter response) {
        // setez header raspuns
        auto m1 = MIME(Application, Json);
        response.setMime(m1);

        Guard guard(lightLock);

        // preiau json-ul din request
        const std::string rawJson =  request.body();
        const auto rawJsonLength = static_cast<int>(rawJson.length());
        

        // creez un obiect json pentru a facilita schimbul de informatie
        Json::Value root;
        JSONCPP_STRING err;
        Json::CharReaderBuilder builderr;

        const std::unique_ptr<Json::CharReader> reader(builderr.newCharReader());

        if (!reader->parse(rawJson.c_str(), rawJson.c_str() + rawJsonLength, &root,
                        &err)) {
        std::cout << "error" << std::endl;
    
        }
        
        bool stareBec = root["stareBec"].asBool();
        int intensitate;
        lightB.setStareBec(stareBec);
        if (!root.isMember("intensitate"))
        {
            time_t now = time(0);
            tm *ltm = localtime(&now);
            int ora = 3 + ltm->tm_hour;
            
            if (7 <= ora && ora <= 16)
            {
                intensitate = 50;
            }else if (16 < ora && ora <= 21)
            {
                intensitate = 75;
            } else {
                intensitate = 100;
            }
        }
        else
        {
            intensitate = root["intensitate"].asInt();
        }
        lightB.setIntensitate(intensitate);

        Json::Value raspuns;
        raspuns["stareBec"] = lightB.getStareBec();
        raspuns["intensitate"] = lightB.getIntensitate();

        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, raspuns);


        response.send(Http::Code::Ok, json_file);
    }

    void colors(const Rest::Request& request, Http::ResponseWriter response){
        // setez header raspuns
        auto m1 = MIME(Application, Json);
        response.setMime(m1);

        Guard guard(lightLock);

        // preiau json-ul din request
        const std::string rawJson =  request.body();
        const auto rawJsonLength = static_cast<int>(rawJson.length());
        

        // creez un obiect json pentru a facilita schimbul de informatie
        Json::Value root;
        JSONCPP_STRING err;
        Json::CharReaderBuilder builderr;

        const std::unique_ptr<Json::CharReader> reader(builderr.newCharReader());

        if (!reader->parse(rawJson.c_str(), rawJson.c_str() + rawJsonLength, &root,
                        &err)) {
        std::cout << "error" << std::endl;
    
        }
        
        Culoare culoare;
        
        if(!lightB.getStareBec()) {
            lightB.setStareBec(true);
        }

        lightB.setIntensitate(100);

        int rgb[3];
        rgb[0] = root["culoare"]["rgb"][0].asInt();
        rgb[1] = root["culoare"]["rgb"][1].asInt();
        rgb[2] = root["culoare"]["rgb"][2].asInt();
        culoare.setHex(root["culoare"]["hex"].asString());
        culoare.setRgb(rgb);
        lightB.setCuloare(culoare);

        Json::Value raspuns;
        raspuns["stareBec"] = lightB.getStareBec();
        raspuns["intensitate"] = lightB.getIntensitate();
        raspuns["culoare"]["hex"] = lightB.getCuloare().getHex();
        raspuns["culoare"]["rgb"][0] = lightB.getCuloare().getRgb()[0];
        raspuns["culoare"]["rgb"][1] = lightB.getCuloare().getRgb()[1];
        raspuns["culoare"]["rgb"][2] = lightB.getCuloare().getRgb()[2];

        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, raspuns);

        response.send(Http::Code::Ok, json_file);
    }

    void music(const Rest::Request& request, Http::ResponseWriter response) {
        // setez header raspuns
        auto m1 = MIME(Application, Json);
        response.setMime(m1);

        Guard guard(lightLock);

        // preiau json-ul din request
        const std::string rawJson =  request.body();
        const auto rawJsonLength = static_cast<int>(rawJson.length());

        // creez un obiect json pentru a facilita schimbul de informatie
        Json::Value root;
        JSONCPP_STRING err;
        Json::CharReaderBuilder builderr;

        const std::unique_ptr<Json::CharReader> reader(builderr.newCharReader());

        if (!reader->parse(rawJson.c_str(), rawJson.c_str() + rawJsonLength, &root,
                        &err)) {
        std::cout << "error" << std::endl;
    
        }

        Muzica m(root["muzica"]["durata"].asInt(), root["muzica"]["bpm"].asInt(), root["muzica"]["ritm"].asInt());
        lightB.setMuzica(m);
        int no_commands;
        int interv;
        int ritm = root["muzica"]["ritm"].asInt();
        if (ritm >= 0 && ritm <= 30){
            no_commands = m.getDurata() / 3;
            interv = 3;
        } else if (ritm > 30 && ritm <= 60){
            no_commands = m.getDurata() / 2;
            interv = 2;
        } else {
            no_commands = m.getDurata();
            interv = 1;
        }

        if (m.getBpm() > 180){
            m.setBpm(180);
        }

        int intensitate = m.getBpm() * 100 / 180;


        Json::Value output;
        for (int i = 0; i < no_commands; ++i){
            int r_val, g_val, b_val;
            r_val = rand() % 255;
            g_val = rand() % 255;
            b_val = rand() % 255;
            Culoare c; //Construieste obiect culoare
            int rgb[] = {r_val, g_val, b_val};
            c.setRgb(rgb);

            int hex_val = ((r_val & 0xff) << 16) + ((g_val & 0xff) << 8) + (b_val & 0xff);
            std::stringstream ss;
            ss << hex_val;
            c.setHex(ss.str());
            lightB.setStareBec(true);
            lightB.setIntensitate(intensitate);
            lightB.setCuloare(c);

            Json::Value raspuns;
            raspuns["stareBec"] = lightB.getStareBec();
            raspuns["culoare"]["hex"] = lightB.getCuloare().getHex();
            raspuns["culoare"]["rgb"][0] = lightB.getCuloare().getRgb()[0];
            raspuns["culoare"]["rgb"][1] = lightB.getCuloare().getRgb()[1];
            raspuns["culoare"]["rgb"][2] = lightB.getCuloare().getRgb()[2];
            raspuns["intensitate"] = lightB.getIntensitate();
            raspuns["interval"] = interv;

            output[i] = raspuns;
            
            //Append raspuns la vectorul de jsoane

        }

        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, output);
        
        response.send(Http::Code::Ok, json_file);
    }

    void alarma(const Rest::Request& request, Http::ResponseWriter response) {
        // setez header raspuns
        auto m1 = MIME(Application, Json);
        response.setMime(m1);

        Guard guard(lightLock);

        // preiau json-ul din request
        const std::string rawJson =  request.body();
        const auto rawJsonLength = static_cast<int>(rawJson.length());
        

        // creez un obiect json pentru a facilita schimbul de informatie
        Json::Value root;
        JSONCPP_STRING err;
        Json::CharReaderBuilder builderr;

        const std::unique_ptr<Json::CharReader> reader(builderr.newCharReader());

        if (!reader->parse(rawJson.c_str(), rawJson.c_str() + rawJsonLength, &root,
                        &err)) {
        std::cout << "error" << std::endl;
    
        }
        
        bool senzorMiscare = root["alarma"]["senzorMiscare"].asBool();
        bool apelUrgenta = root["alarma"]["apelUrgenta"].asBool();
        Alarma a = new Alarma(senzorMiscare,apelUrgenta);

        lightB.setAlarma(a);
        lightB.setStareBec(true);
    
        Json::Value raspuns;
        raspuns["culoare"]["hex"] = lightB.getAlarma().getCuloareAlarma().getHex();
        raspuns["culoare"]["rgb"][0] = lightB.getCuloare().getRgb()[0];
        raspuns["culoare"]["rgb"][1] = lightB.getCuloare().getRgb()[1];
        raspuns["culoare"]["rgb"][2] = lightB.getCuloare().getRgb()[2];
        raspuns["alarma"]["senzorMiscare"] = lightB.getAlarma().getSenzorMiscare();
        raspuns["alarma"]["apelUrgenta"] = lightB.getAlarma().getApelUrgenta();
        raspuns["stareBec"] = lightB.getStareBec();
        raspuns["intensitate"] = lightB.getAlarma().getIntensitateAlarma();

        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, raspuns);


        response.send(Http::Code::Ok, json_file);
    }

    void intensity(const Rest::Request& request, Http::ResponseWriter response) {
        // setez header raspuns
        auto m1 = MIME(Application, Json);
        response.setMime(m1);

        Guard guard(lightLock);

        // preiau json-ul din request
        const std::string rawJson =  request.body();
        const auto rawJsonLength = static_cast<int>(rawJson.length());
        

        // creez un obiect json pentru a facilita schimbul de informatie
        Json::Value root;
        JSONCPP_STRING err;
        Json::CharReaderBuilder builderr;

        const std::unique_ptr<Json::CharReader> reader(builderr.newCharReader());

        if (!reader->parse(rawJson.c_str(), rawJson.c_str() + rawJsonLength, &root,
                        &err)) {
            std::cout << "error" << std::endl;
        }

        int intensitate = root["intensitate"].asInt();
        lightB.setIntensitate(intensitate);
        lightB.setStareBec(true);

        Json::Value raspuns;
        raspuns["intensitate"] = lightB.getIntensitate();
        raspuns["stareBec"] = lightB.getStareBec();
        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, raspuns);

        response.send(Http::Code::Ok, json_file);
    }

    void getStatus(const Rest::Request& request, Http::ResponseWriter response) {
        // setez header raspuns
        auto m1 = MIME(Application, Json);
        response.setMime(m1);

        Json::Value raspuns;
        raspuns["stareBec"] = lightB.getStareBec();

        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, raspuns);

        response.send(Http::Code::Ok, json_file);
    }

    void getIntensity(const Rest::Request& request, Http::ResponseWriter response) {
        // setez header raspuns
        auto m1 = MIME(Application, Json);
        response.setMime(m1);

        Json::Value raspuns;
        raspuns["intensitate"] = lightB.getIntensitate();

        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, raspuns);

        response.send(Http::Code::Ok, json_file);
    }

    void getCuloare(const Rest::Request& request, Http::ResponseWriter response) {
        // setez header raspuns
        auto m1 = MIME(Application, Json);
        response.setMime(m1);

        if(lightB.getCuloare().getRgb() == NULL && lightB.getCuloare().getHex().empty()) {
            std::string error = "Error: the light bulb color has not been set previously.";
            std::cout << error << std::endl;

            Json::Value raspuns;
            raspuns["error"] = error;

            Json::StreamWriterBuilder builder;
            const std::string json_file = Json::writeString(builder, raspuns);
        
            response.send(Http::Code::Ok, json_file);
            return;
        }

        Json::Value raspuns;
        raspuns["culoare"]["rgb"][0] = lightB.getCuloare().getRgb()[0];
        raspuns["culoare"]["rgb"][1] = lightB.getCuloare().getRgb()[1];
        raspuns["culoare"]["rgb"][2] = lightB.getCuloare().getRgb()[2];
        raspuns["culoare"]["hex"] = lightB.getCuloare().getHex();

        Json::StreamWriterBuilder builder;
        const std::string json_file = Json::writeString(builder, raspuns);

        response.send(Http::Code::Ok, json_file);
    }

    // Create the lock which prevents concurrent editing of the same variable
    using Lock = std::mutex;
    using Guard = std::lock_guard<Lock>;
    Lock lightLock;

    // Instance of the Light model
    Light lightB;
    

    // Defining the httpEndpoint and a router.
    std::shared_ptr<Http::Endpoint> httpEndpoint;
    Rest::Router router;
};

int main(int argc, char *argv[]) {

    // This code is needed for gracefull shutdown of the server when no longer needed.
    sigset_t signals;
    if (sigemptyset(&signals) != 0
            || sigaddset(&signals, SIGTERM) != 0
            || sigaddset(&signals, SIGINT) != 0
            || sigaddset(&signals, SIGHUP) != 0
            || pthread_sigmask(SIG_BLOCK, &signals, nullptr) != 0) {
        perror("install signal handler failed");
        return 1;
    }

    // Set a port on which your server to communicate
    Port port(9080);

    // Number of threads used by the server
    int thr = 2;

    if (argc >= 2) {
        port = static_cast<uint16_t>(std::stol(argv[1]));

        if (argc == 3)
            thr = std::stoi(argv[2]);
    }

    Address addr(Ipv4::any(), port);

    std::cout << "Cores = " << hardware_concurrency() << std::endl;
    std::cout << "Using " << thr << " threads" << std::endl;

    // Instance of the class that defines what the server can do.
    LightEndpoint stats(addr);

    // Initialize and start the server
    stats.init(thr);
    stats.start();


    // Code that waits for the shutdown sinal for the server
    int signal = 0;
    int status = sigwait(&signals, &signal);
    if (status == 0)
    {
        std::cout << "received signal " << signal << std::endl;
    }
    else
    {
        std::cerr << "sigwait returns " << status << std::endl;
    }

    stats.stop();
}