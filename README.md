# Responsive light bulbs #

Aplicație pentru comunicarea cu o rețea de becuri inteligente. 

## Echipa ##
- Achim Luiza-Elena
- Schipor Ioana-Teodora
- Gioancă Raul-Dumitru
- Teșileanu Alexandru-Gabriel
- Pietrăreanu George-Caludiu
- Sugeac Andrei

## Dependencies ##

Aplicația a fost dezvoltată cu ajutorul framework-ului web [Pistache](http://pistache.io/docs/). Codul este compilat cu ajutorul unui compiler ce suportă C++ 17 (noi am uitlizat g++). Pentru utilizarea formatului JSON în cadrul serviciilor am utilizat [jsoncpp](https://github.com/open-source-parsers/jsoncpp).

Pentru a putea instala dependency-urile este de preferat să clonați repo-ul local prin comanda: git clone https://Raul-D-G@bitbucket.org/Raul-D-G/responsive-light-bulbs..git

## Comanda de build ##

Pentru realizarea build-ului, trebuie deschis un terminal în folderul în care se află proiectul și rulată următoarea comandă:

```
g++ -std=c++17 responsiveLightBulbs.cpp Source\ Files/culoare.cpp Source\ Files/muzica.cpp Source\ Files/alarma.cpp Source\ Files/light.cpp -o responsiveLightBulbs -lpistache -lcrypto -lssl -lpthread -ljsoncpp
```

## Start server ##

Serverul se porneste prin comanda următoare:

```
./responsiveLightBulbs
```

## Testarea rutelor ##

Rutele pot fi testate cu PostMan.

## Raport de analiză ##

Raportul de analiză îl puteți găsi prin accesarea acestui [link](https://drive.google.com/file/d/1Rntwrp8gtse__RLpa3-Mu1dmOVWxPQLF/view?usp=sharing)