#pragma once

#include <iostream>
#include "../Header Files/culoare.h"
#include "../Header Files/alarma.h"
#include "../Header Files/muzica.h"

class Light {
private:
    Culoare m_culoare;
    Muzica m_muzica;
    Alarma m_alarma;
    bool m_stareBec;
    int m_intensitate;
    
public:
    Light(bool stareBec = false, int intensitate = 0);

    Culoare getCuloare() const;
    void setCuloare(Culoare culoare);

    Muzica getMuzica() const;
    void setMuzica(Muzica muzica);

    Alarma getAlarma() const;
    void setAlarma(Alarma alarma);

    bool getStareBec() const;
    void setStareBec(bool stareBec);

    int getIntensitate() const;
    void setIntensitate(int intensitate);

    friend std::ostream& operator<<(std::ostream& stream, const Light& light);
};

std::ostream& operator<<(std::ostream& stream, const Light& light);