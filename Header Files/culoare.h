#pragma once

#include <iostream>
#include <string>

class Culoare {
public:
    static const int RGB_SIZE= 3;
    static const int HEX_SIZE= 7;
private:
    int m_rgb [RGB_SIZE]= {0, 0, 255};
    std::string m_hex = "#4287f5";
    
public:
 
    Culoare() = default;
    Culoare(int rgb [RGB_SIZE] , std::string hex = "#4287f5" );
    Culoare(const Culoare&);

    const int * getRgb() const;
    void setRgb(int * rgb);

    std::string getHex() const;
    void setHex(std::string hex);
    
    friend std::ostream& operator<<(std::ostream& stream, const Culoare& culoare);
};

std::ostream& operator<<(std::ostream& stream, const Culoare& culoare);