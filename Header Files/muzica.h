#pragma once

#include <iostream>

class Muzica {
private:
    int m_durata, m_bpm, m_ritm;
    
public:
    Muzica(int durata = 0, int bpm = 0, int ritm = 0);
    Muzica(const Muzica&);

    int getDurata() const;
    void setDurata(int durata); 

    int getBpm() const; 
    void setBpm(int bpm); 

    int getRitm() const; 
    void setRitm(int ritm); 

    friend std::ostream& operator<<(std::ostream& stream, const Muzica& muzica);
};

std::ostream& operator<<(std::ostream& stream, const Muzica& muzica);