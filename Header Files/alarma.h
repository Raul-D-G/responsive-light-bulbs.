#pragma once

#include <iostream>
#include "culoare.h"


class Alarma {

private:
    bool m_senzorMiscare, m_apelUrgenta;
    Culoare m_culoareAlarma;
    int m_intensitateAlarma;
public:
 
    Alarma(bool senzorMiscare = true, bool apelUrgenta = true);
    Alarma(const Alarma&);

    bool getSenzorMiscare() const;
    void setSenzorMiscare(bool senzorMiscare);

    bool getApelUrgenta() const;
    void setApelUrgenta(bool apelUrgenta);

    Culoare getCuloareAlarma() const;
    int getIntensitateAlarma() const;

    friend std::ostream& operator<<(std::ostream& stream, const Alarma& alarma);
};

std::ostream& operator<<(std::ostream& stream, const Alarma& alarma);