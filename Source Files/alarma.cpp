
#include "../Header Files/alarma.h"

#include "../Header Files/culoare.h"


Alarma::Alarma(bool senzorMiscare, bool apelUrgenta)
    : m_senzorMiscare(senzorMiscare)
    , m_apelUrgenta(apelUrgenta)
{
    int rgb [3] = {255, 0, 0};
    Culoare c (rgb,(std::string)"#FF0000");
    m_culoareAlarma = c;
    m_intensitateAlarma = 100;
}

Alarma::Alarma(const Alarma& alarma)
    : m_senzorMiscare(alarma.getSenzorMiscare())
    , m_apelUrgenta(alarma.getApelUrgenta())
    , m_intensitateAlarma(alarma.getIntensitateAlarma())
{
    m_culoareAlarma = alarma.getCuloareAlarma();
}

bool Alarma::getSenzorMiscare() const { return m_senzorMiscare; }
void Alarma::setSenzorMiscare(bool senzorMiscare) { m_senzorMiscare = senzorMiscare; }

bool Alarma::getApelUrgenta() const { return m_apelUrgenta; }
void Alarma::setApelUrgenta(bool apelUrgenta) { m_apelUrgenta = apelUrgenta; }

Culoare Alarma::getCuloareAlarma() const { return m_culoareAlarma; }
int Alarma::getIntensitateAlarma() const { return m_intensitateAlarma; }

std::ostream& operator<<(std::ostream& stream, const Alarma& alarma)
{
    const bool senzorMiscare= alarma.getSenzorMiscare();
    const bool apelUrgenta = alarma.getApelUrgenta();
    const Culoare culoare = alarma.getCuloareAlarma();
    const int intensitate = alarma.getIntensitateAlarma();
    stream << senzorMiscare << " "<<  apelUrgenta<< " "<<culoare<< " " <<" "<<intensitate<<std::endl; 
    return stream;
}