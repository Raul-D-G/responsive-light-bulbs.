
#include "../Header Files/culoare.h"

Culoare::Culoare(int rgb [RGB_SIZE], std::string hex ) {
    for (int i = 0; i < RGB_SIZE; i++)
    {
        m_rgb[i] = rgb[i];
    }

    this->m_hex = hex;
}

Culoare::Culoare(const Culoare& culoare) {
    for (int i = 0; i < Culoare::RGB_SIZE; i++)
    {
        m_rgb[i] = culoare.m_rgb[i];
    }
    m_hex = culoare.getHex();
}

const int * Culoare::getRgb() const {
    return m_rgb;
}

void Culoare::setRgb(int * rgb) {
    for (int i = 0; i < RGB_SIZE; i++)
    {
        m_rgb[i] = rgb[i];   
    }
}

std::string Culoare::getHex() const{
    return this->m_hex;
}

void Culoare::setHex(std::string hex) {
    m_hex = hex;
}

std::ostream& operator<<(std::ostream& stream, const Culoare& culoare)
{
    const int* toShow= culoare.getRgb();
    for (int i= 0; i < Culoare::RGB_SIZE; i++) {
        stream << toShow[i] << ' ';
    }
    stream << std::endl;
    std::string toS = culoare.getHex();
    stream << toS << std::endl;
    return stream;
}