
#include "../Header Files/light.h"

#include <iostream>
#include <string>

Light::Light(bool stareBec, int intensitate)
    : m_stareBec(stareBec)
    , m_intensitate(intensitate)    
{
    Culoare culoare;
    Muzica muzica;
    Alarma alarma;
    m_culoare = culoare;
    m_muzica = muzica;
    m_alarma = alarma;
}

bool Light::getStareBec() const { return m_stareBec; }
void Light::setStareBec(bool stareBec) { m_stareBec = stareBec; }

int Light::getIntensitate() const { return m_intensitate; }
void Light::setIntensitate(int intensitate) { m_intensitate = intensitate; }

Culoare Light::getCuloare() const { return this->m_culoare; }
void Light::setCuloare(Culoare culoare) {
    m_culoare = culoare;
}

Muzica Light::getMuzica() const { return this->m_muzica; }
void Light::setMuzica(Muzica muzica) {
    m_muzica = muzica;
}

Alarma Light::getAlarma() const { return this->m_alarma; }
void Light::setAlarma(Alarma alarma) {
    m_alarma = alarma;
}

std::ostream& operator<<(std::ostream& stream, const Light& light)
{
    stream <<light.getCuloare() << light.getMuzica() << light.getAlarma() << std::endl;
    stream << "Stare bec : " << light.getStareBec() << " "  << "Intensitate:" << light.getIntensitate()<<std::endl;

    return stream;
}