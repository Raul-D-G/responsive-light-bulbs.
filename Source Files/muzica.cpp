
#include "../Header Files/muzica.h"


Muzica::Muzica(int durata, int bpm, int ritm)
    : m_durata(durata)
    , m_bpm(bpm)
    , m_ritm(ritm)
{ }

Muzica::Muzica(const Muzica& muzica)
    : m_durata(muzica.getDurata())
    , m_bpm(muzica.getBpm())
    , m_ritm(muzica.getRitm()) 
{ }

int Muzica::getDurata() const { return m_durata; }
void Muzica::setDurata(int durata) { m_durata = durata; }

int Muzica::getBpm() const { return m_bpm; }
void Muzica::setBpm(int bpm) { m_bpm = bpm; }

int Muzica::getRitm() const { return m_ritm; }
void Muzica::setRitm(int ritm) { m_ritm = ritm; }

std::ostream& operator<<(std::ostream& stream, const Muzica& muzica)
{
    const int durata= muzica.getDurata();
    const int bpm = muzica.getBpm();
    const int ritm = muzica.getRitm();
    
    stream << "Durata : " << durata << " " << "Bpm : " << bpm << " " << "Ritm : " << ritm << std::endl; 
    return stream;
}
